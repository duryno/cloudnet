/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cloudnet;

import cloudnet.user.User;
import cloudnet.user.UserDropBox;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;

/**
 *
 * @author davidmunro
 */
public class UserHomeController implements Initializable {

    static String accessToken;

    @FXML
    Button button, activateButton, addCloud;
    @FXML
    AnchorPane content;
    @FXML
    AnchorPane anc;
    @FXML
    public static TextField tokenTextField;
    @FXML
    public static ListView listFolders;
    @FXML
    public static TextField directoryText;
    @FXML
    public static ContextMenu choiceMenu;
    @FXML
    public static MenuItem copy, paste, download, fileFolderProperties;
    
    @FXML
    public static Label infoLabel;
    
    CloudNet cloud = new CloudNet();
    UserDropBox drop = new UserDropBox(accessToken);

    final String keyForApp = "3arl279eij5125u";
    final String secretKeyForApp = "ic83wodtpty04ut";
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //can check here if there is any tokens stored.
//        if(!User.getDropboxToken().isEmpty()){
//            System.out.println("there is a dropbox token");
//            //then call the add dropbox method to add the dropbox UserDropBox.showCloud(User.getDropBoxToken());
//        }
   }

    @FXML
    public void addCloud() throws IOException, DbxException {

        Runnable run = new Runnable() {

            @Override
            public void run() {
                activateButton.setVisible(true);
                tokenTextField.setVisible(true);

                drop.intialDropboxSetup(keyForApp, secretKeyForApp);

            }
        };
        Thread thr = new Thread(run);
        thr.setDaemon(true); //so when the main program ends, so will this thread.
        thr.start();

    }

    @FXML
    public void showCloud() throws DbxException, IOException {
        drop.showCloud(tokenTextField.getText());
    }

    public void logout() throws IOException {
        User.setDropboxToken(null);
        User.setGoogleToken(null);
        User.setOneDriveToken(null);
        User.setID(null);
        cloud.logUserOut();
    }

    @FXML
    public void setListItems(ObservableList ol) {
        listFolders.setItems(ol);
    }
    
    @FXML
    public void clearTextView(){
        tokenTextField.clear();
    }
    
    @FXML
    public void setDirectoryText(String currentDir){
        directoryText.setText(currentDir);
    }
    
    @FXML
    public static void setInfoLabel(String textToDisplay){
        infoLabel.setText(textToDisplay);
    }
}
